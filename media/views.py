from django.shortcuts import render
from media.models import Image
from media.models import Video

def affichage(request):
    galerie = Image.objects.all()
    videos = Video.objects.all()
    return render(request, 'media/images.html', locals())
