# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0005_auto_20150917_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='jour',
            field=models.BooleanField(verbose_name=b'Image du jour'),
        ),
    ]
