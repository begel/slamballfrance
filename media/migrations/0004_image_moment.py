# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0003_auto_20150904_0833'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='moment',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
