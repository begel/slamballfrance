# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0004_image_moment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='moment',
        ),
        migrations.AddField(
            model_name='image',
            name='jour',
            field=models.BooleanField(default=False, verbose_name=b'Image du jour'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='video',
            name='jour',
            field=models.BooleanField(default=False, verbose_name=b'Video du moment'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='image',
            name='fond',
            field=models.BooleanField(default=True, verbose_name=b'Image utilisable en fond'),
        ),
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.ImageField(upload_to=b'images_site/'),
        ),
    ]
