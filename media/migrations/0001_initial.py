# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'jpg/')),
                ('miniature', models.ImageField(upload_to=b'miniatures/')),
                ('titre', models.CharField(max_length=50)),
                ('auteur', models.CharField(max_length=50, null=True, verbose_name=b'Auteur de la photo', blank=True)),
                ('date', models.DateTimeField(null=True, verbose_name=b'Date de prise de vue', blank=True)),
                ('legende', models.TextField(null=True, blank=True)),
                ('lieu', models.CharField(max_length=100, null=True, blank=True)),
                ('fond', models.BooleanField(default=True)),
                ('jour', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_ogg', models.FileField(upload_to=b'Videos/ogg/', verbose_name=b'Video au format ogv')),
                ('video_webm', models.FileField(upload_to=b'Videos/webm/', null=True, verbose_name=b'Video au format webm')),
                ('video_mp4', models.FileField(upload_to=b'Videos/mp4/', verbose_name=b'Video au format mp4')),
                ('apercu', models.ImageField(upload_to=b'Videos/apercu/', verbose_name=b'Image de la video')),
                ('titre', models.CharField(max_length=50)),
                ('auteur', models.CharField(max_length=50, verbose_name=b'Auteur de la video', blank=True)),
                ('date', models.DateTimeField(null=True, verbose_name=b'Date de prise de vue')),
                ('legende', models.TextField(blank=True)),
                ('lieu', models.CharField(max_length=100, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
