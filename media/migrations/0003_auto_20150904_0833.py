# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0002_video_jour'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='jour',
        ),
        migrations.RemoveField(
            model_name='video',
            name='jour',
        ),
    ]
