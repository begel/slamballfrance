from django.db import models
from os import path


class Image(models.Model):
    image = models.ImageField(upload_to="images_site/")
    miniature = models.ImageField(upload_to="miniatures/")
    titre = models.CharField(max_length=50)
    auteur = models.CharField(max_length=50, null=True, blank=True, verbose_name="Auteur de la photo")
    date = models.DateTimeField(auto_now_add=False, verbose_name="Date de prise de vue", null=True, blank=True)
    legende = models.TextField(null=True, blank=True)    
    lieu = models.CharField(max_length=100, null=True, blank=True)
    fond = models.BooleanField(default=True, verbose_name="Image utilisable en fond")
    jour = models.BooleanField(verbose_name="Image du jour")    

    def __unicode__(self):
        return u'%s' %(self.titre)
    
    def save(self, *args, **kwargs):
        from PIL import Image as im
        from cStringIO import StringIO
        from django.core.files.uploadedfile import SimpleUploadedFile
        
        def create_thumb(image, size):
            """Returns the image resized to fit inside a box of the given size"""
            image.thumbnail(size, im.ANTIALIAS)
            temp = StringIO()
            image.save(temp, 'png')
            temp.seek(0)
            return SimpleUploadedFile('temp', temp.read(),content_type='image/png')

        def has_changed(instance, field, manager='objects'):
            """Returns true if a field has changed in a model
        	May be used in a model.save() method.
        	"""
            if not instance.pk:
                return True
            manager = getattr(instance.__class__, manager)
            old = getattr(manager.get(pk=instance.pk), field)
            return not getattr(instance, field) == old

        if has_changed(self, 'image'):
            # on va convertir l'image en jpg
            filename = path.splitext(path.split(self.image.name)[-1])[0]
            filename = "%s.jpg" % filename

            image = im.open(self.image.file)

            if image.mode not in ('L', 'RGB'):
                image = image.convert('RGB')

            #le thumbnail
            self.miniature.save(
                    filename,
                    create_thumb(image, (250,250)),
                    save=False)
        super(Image,self).save() 
        

class Video(models.Model):
    video_ogg = models.FileField(upload_to="Videos/ogg/",verbose_name="Video au format ogv")
    video_webm = models.FileField(upload_to="Videos/webm/",verbose_name="Video au format webm",null=True)
    video_mp4 = models.FileField(upload_to="Videos/mp4/",verbose_name="Video au format mp4")
    apercu = models.ImageField(upload_to="Videos/apercu/",verbose_name="Image de la video")
    titre = models.CharField(max_length=50)
    auteur = models.CharField(max_length=50, blank=True,verbose_name="Auteur de la video")
    date = models.DateTimeField(auto_now_add=False, verbose_name="Date de prise de vue", null=True)
    legende = models.TextField(blank=True)    
    lieu = models.CharField(max_length=100, blank=True)
    jour = models.BooleanField(default=False, verbose_name="Video du moment")
    
    def __str__(self):
        return u'%s' %(self.titre)

