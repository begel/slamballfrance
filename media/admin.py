from django.contrib import admin
from media.models import Image, Video

class ImageAdmin(admin.ModelAdmin):
   list_display   = ('titre', 'auteur',)
   list_filter    = ('titre',)
   ordering       = ('titre', )
   search_fields  = ('titre', 'auteur')
   fields = ['image','titre','auteur','date','legende','lieu','fond', 'jour']

admin.site.register(Image, ImageAdmin)
admin.site.register(Video)
