
# -*- coding: utf-8 -*-
from django.db import models

class Question(models.Model):
    question = models.TextField()
    auteurQ = models.CharField(max_length=42)
    reponse = models.TextField(null=True, blank=True)
    auteurR = models.CharField(max_length=42, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, 
                                verbose_name="Date de parution")
    mail = models.EmailField(null=True, blank=True)
    
    def __unicode__(self):
        return u'%s' %(self.question)