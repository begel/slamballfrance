import datetime

from django.utils import timezone
from django.test import TestCase

from .models import Publication


class PublicationMethodTests(TestCase):

    def test_recent_with_future_question(self):
        """
        recent() should return False for publication whose
        date is in the future.
        """
        time = timezone.now() + datetime.timedelta(years=1)
        future_publication = Publication(date=time)
        self.assertIs(future_publication.recent(), False)
