
# -*- coding: utf-8 -*-
from django.shortcuts import render
from faq.forms import PoserQ
from faq.models import Question

def poserQ(request):
    if request.method == 'POST':  # S'il s'agit d'une requête POST
        form = PoserQ(request.POST)  # Nous reprenons les données
        if form.is_valid(): # Nous vérifions que les données envoyées sont valides
            # Ici nous pouvons traiter les données du formulaire
            auteur = form.cleaned_data['auteur']
            mail = form.cleaned_data['mail']
            question = form.cleaned_data['question']
            data = Question(auteurQ=auteur, question=question, mail=mail)
            data.save()
            # Nous pourrions ici envoyer l'e-mail grâce aux données que nous venons de récupérer
            envoi = True
            form = PoserQ()
    else: # Si ce n'est pas du POST, c'est probablement une requête GET
        form = PoserQ()  # Nous créons un formulaire vide
    questions = Question.objects.order_by('-date')
    return render(request, 'faq/questions.html', locals())
