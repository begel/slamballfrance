
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import ListView
from faq.models import Question

urlpatterns = patterns('',
   # url(r'^questions$',ListView.as_view(model=Question,template_name='faq/questions.html',context_object_name="questions"),name="questions"),
    url(r'^questions$', 'faq.views.poserQ', name="questions"),
)