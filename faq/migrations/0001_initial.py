# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('auteurQ', models.CharField(max_length=42)),
                ('reponse', models.TextField(null=True)),
                ('auteurR', models.CharField(max_length=42)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name=b'Date de parution')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
