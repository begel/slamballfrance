# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0005_auto_20150816_0914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='mail',
            field=models.EmailField(max_length=75, null=True, blank=True),
        ),
    ]
