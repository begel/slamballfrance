# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0004_auto_20150724_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='mail',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
    ]
