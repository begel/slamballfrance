# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0002_auto_20150723_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='mail',
            field=models.EmailField(max_length=75, null=True),
            preserve_default=True,
        ),
    ]
