# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0003_question_mail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='auteurR',
            field=models.CharField(max_length=42, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='mail',
            field=models.EmailField(max_length=75, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='reponse',
            field=models.TextField(null=True, blank=True),
        ),
    ]
