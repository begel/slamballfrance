
# -*- coding: utf-8 -*-
from django import forms

class PoserQ(forms.Form):
    auteur = forms.CharField(max_length=50, label="Votre nom ")
    mail = forms.EmailField(label="Votre adresse mail ", required=False)
    question = forms.CharField(widget=forms.Textarea, label="Votre question ")

