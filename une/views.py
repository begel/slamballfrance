#-*-coding: utf-8 -*-
from django.shortcuts import render

from datetime import datetime
from django.shortcuts import render
from une.models import Article
from faq.models import Question
from media.models import Image, Video

def accueil(request):
    actualites = Article.objects.order_by('-date')
    une = actualites[0]
    last_questions = Question.objects.order_by('-date')[:3]
    fonds = Image.objects.filter(fond=True)
    image_jour = Image.objects.filter(jour=True)[0]
    video_jour = Video.objects.filter(jour=True)[0]
    return render(request, 'une/accueil.html', locals())
