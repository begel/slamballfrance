# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('une', '0003_article_image'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Article',
        ),
    ]
