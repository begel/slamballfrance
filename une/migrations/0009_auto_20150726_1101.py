# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('une', '0008_auto_20150724_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='contenu',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='article',
            name='image',
            field=models.ImageField(upload_to=b'une/static/une/jpg', blank=True),
        ),
    ]
