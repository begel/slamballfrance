# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('une', '0007_article_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='contenu',
            field=models.TextField(null=True, blank=True),
        ),
    ]
