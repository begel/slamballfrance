# -*- coding: utf-8 -*-
from django.db import models

class Article(models.Model):
    titre = models.CharField(max_length=100)
    auteur = models.CharField(max_length=42)
    contenu = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False, 
                                verbose_name="Date de parution")
    image = models.ImageField(upload_to="une/Images/", blank=True)
    
    def __unicode__(self):
        return u'%s' %(self.titre)
