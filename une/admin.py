from django.contrib import admin
from une.models import Article

admin.site.register(Article) 
admin.autodiscover()