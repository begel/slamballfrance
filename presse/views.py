from django.shortcuts import render
from presse.models import ArticleJ
from media.models import Image

def presse2(request):
    articlesJ = ArticleJ.objects.order_by('-dateS')
    articlesJTexte = ArticleJ.objects.order_by('-dateS')
    fonds = Image.objects.filter(fond=True).order_by('titre')    
    n = len(fonds)
    m = len(articlesJTexte)
    if n<m:
        k = m/n
        fonds_2 = []
        for i in range(k+1):
            for j in fonds:
                fonds_2.append(j)
    else:
        fonds_2 = fonds
        
    couple = [(a,fonds_2[i]) for i,a in enumerate(articlesJTexte)]
    fonds = fonds.reverse()
    return render(request, 'presse/revue.html', locals())
    
def presse(request):
    articlesJ = ArticleJ.objects.order_by('-dateJ')
    fonds = Image.objects.filter(fond=True).order_by('titre')  
    fonds = fonds.reverse()
    return render(request, 'presse/revue.html', locals())
