# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presse', '0003_auto_20150724_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlej',
            name='contenu',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='dateJ',
            field=models.DateField(default=None, null=True, verbose_name=b'Date de parution', blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b'Images/', blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='journaliste',
            field=models.CharField(default=None, max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='lien',
            field=models.URLField(default=None, null=True, blank=True),
        ),
    ]
