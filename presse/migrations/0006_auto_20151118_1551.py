# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presse', '0005_auto_20150726_1021'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='articlej',
            name='contenu',
        ),
        migrations.AlterField(
            model_name='articlej',
            name='dateJ',
            field=models.DateField(verbose_name=b'Date de parution'),
        ),
    ]
