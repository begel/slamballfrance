# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presse', '0004_auto_20150726_1015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlej',
            name='contenu',
            field=models.TextField(default=None, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='image',
            field=models.ImageField(default=None, upload_to=b'Images/', blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='journaliste',
            field=models.CharField(default=None, max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='lien',
            field=models.URLField(default=None, blank=True),
        ),
    ]
