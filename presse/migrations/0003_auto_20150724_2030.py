# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presse', '0002_auto_20150724_1929'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlej',
            name='contenu',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='dateJ',
            field=models.DateField(null=True, verbose_name=b'Date de parution', blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='image',
            field=models.ImageField(null=True, upload_to=b'Images/', blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='journaliste',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='articlej',
            name='lien',
            field=models.URLField(null=True, blank=True),
        ),
    ]
