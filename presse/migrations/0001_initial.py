# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleJ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('journal', models.CharField(max_length=50, verbose_name=b'Nom du journal')),
                ('typeP', models.CharField(max_length=50, verbose_name=b'Type du journal')),
                ('titre', models.CharField(max_length=100, verbose_name=b"Titre de l'article")),
                ('contenu', models.TextField(null=True)),
                ('journaliste', models.CharField(max_length=50, null=True)),
                ('dateJ', models.DateField(null=True, verbose_name=b'Date de parution')),
                ('dateS', models.DateField(auto_now_add=True, verbose_name=b'Date de publication ici')),
                ('lien', models.URLField(null=True)),
                ('image', models.ImageField(upload_to=b'Images/')),
                ('resume', models.TextField(verbose_name=b"Resume qui s'affiche sur le site en prem's")),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
