# -*- coding: utf-8 -*-
from django.db import models

class ArticleJ(models.Model):
    journal = models.CharField(max_length=50, verbose_name="Nom du journal")
    typeP = models.CharField(max_length=50, verbose_name="Type du journal")
    titre = models.CharField(max_length=100, verbose_name="Titre de l'article")
    journaliste = models.CharField(max_length=50, blank=True, default=None)
    dateJ = models.DateField(auto_now_add=False, verbose_name="Date de parution")
    dateS = models.DateField(auto_now_add=True, verbose_name="Date de publication ici")
    lien = models.URLField(blank=True, default=None)
    image = models.ImageField(upload_to="Images/", blank=True, default=None)
    resume = models.TextField(verbose_name="Resume qui s'affiche sur le site en prem's")

    def __unicode__(self):
        return u'%s' % (self.titre)
