function ajuster_nb_images(id,classe_fond){
	var elmt = document.getElementById(id);
	var dist_haut = elmt.offsetTop + elmt.offsetHeight;
	var imgs = document.getElementsByClassName(classe_fond);
	var nb_imgs = imgs.length;
	var i = nb_imgs-1;
	while((i>=0)&&(imgs[i].offsetTop + imgs[i].offsetHeight >= dist_haut)){
		/* tant que l'image arrive en-dessous de l'élément sélectionné */
		i--;
	}
	/* i contient le numéro de la première image qui arrive trop bas
	* i+1 est la dernière qu'il faut laisser
	* i+2 est la première à enlever
	*/
	for(var k=i+2;k<nb_imgs;k++){
		imgs[k].parentNode.removeChild(imgs[k]);
	}
	/* s'il n'y a pas d'images à enlever */
	if((i==nb_imgs-1)){
		var j=0;
		var b=true;
		while(b){
			var imgs2 = document.getElementsByClassName(classe_fond);
			var img = imgs2[imgs2.length-1];
			if(img.offsetTop + img.offsetHeight < dist_haut){
				/* tant que la dernière image arrive au-dessus de la fin de section */
				var im_prov = imgs[j].cloneNode(true);
				img.parentNode.appendChild(im_prov);
				if(j<i){j++;}
				else{j=0;}
			}
			else{b=false;}
		}
	}
}
