/* fonction à appeler à la fin du chargement du HTML : gestion de la galerie d'images */
function defilement(){
 var i=0;
 var nb=document.querySelectorAll('a[href="#img_grande"]').length;
 /* récupère les données d'une image et les insère dans les emplacements de l'agrandissement */
 function changer_donnees_img(elmt){
  var alt=elmt.getElementsByTagName('img')[0].getAttribute('alt');
  var donnees=elmt.parentNode;
  //console.log(donnees);
  var source=donnees.querySelector('.source');
  var auteur=donnees.querySelector('.auteur');
  var date=donnees.querySelector('.date');
  var lieu=donnees.querySelector('.lieu');
  var legende=donnees.querySelector('.legende');
  if(auteur!=null)auteur='Par '+auteur.textContent;
  else auteur='';
  if(lieu!=null)lieu='A '+lieu.textContent;
  else lieu='';
  if(date!=null)date='Prise le '+date.textContent;
  else date='';
  if(legende!=null)legende=legende.textContent;
  else legende='';
  if(source!=null)source=source.textContent;
  else source='';
  document.querySelector('#img_grande img').setAttribute('src',source)
  document.querySelector('#img_grande img').setAttribute('alt',alt);
  document.querySelector('#img_grande h1').innerHTML = alt;
  document.querySelectorAll('#img_grande p')[0].innerHTML = legende;
  document.querySelectorAll('#img_grande p')[1].innerHTML = date + '<br/>' + auteur + '<br/>' + lieu;
 };

 /* associer aux images le comportement au clic */
 for(var k=0;k<nb;k++)
 	{
 	document.querySelectorAll('a[href="#img_grande"]')[k].onclick=function(){
   		i=k-1;
   		changer_donnees_img(this);
  		};
  	};
 
 /* comportement du défilement droit au clic */
 document.getElementById('img-droite').onclick=function(){
  if (i<nb-1){
   i++;
  }
  else{
   i=0;
  }
  var im=document.querySelectorAll('a[href="#img_grande"]')[i];
  changer_donnees_img(im);
 };
 
 /* comportement du défilement gauche au clic */
 document.getElementById('img-gauche').onclick=function(){
  if (i>0){
   i--;
  }
  else{
   i=nb-1;
  }
  var im=document.querySelectorAll('a[href="#img_grande"]')[i];
  changer_donnees_img(im);
 };

/* gestion des vidéos */
 var j=0;
 var nb_v=document.querySelectorAll('a[href="#video_grande"]').length;
 
 /* récupère les données d'une video et les insère dans les emplacements de l'agrandissement */
 function changer_donnees_video(elmt){
  document.getElementsByTagName('video')[0].pause();
  var apercu=elmt.getElementsByTagName('img')[0].getAttribute('src');
  var alt=elmt.getElementsByTagName('img')[0].getAttribute('alt');
  var donnees=elmt.parentNode;
  var auteur=donnees.getElementsByClassName('auteur')[0];
  var date=donnees.getElementsByClassName('date')[0];
  var lieu=donnees.getElementsByClassName('lieu')[0];
  var legende=donnees.getElementsByClassName('legende')[0];
  var source_ogg = donnees.getElementsByClassName('ogv')[0];
  var source_mp4 = donnees.getElementsByClassName('mp4')[0];
  var source_webm = donnees.getElementsByClassName('webm')[0];
  if(auteur!=null)auteur='Par '+auteur.textContent;
  else auteur='';
  if(lieu!=null)lieu='A '+lieu.textContent;
  else lieu='';
  if(date!=null)date='Prise le '+date.textContent;
  else date='';
  if(legende!=null)legende=legende.textContent;
  else legende='';
  if(source_ogg!=null)source_ogg='/static/'+source_ogg.textContent;
  else source_ogg='';
  if(source_mp4!=null)source_mp4='/static/'+source_mp4.textContent;
  else source_mp4='';
  if(source_webm!=null)source_webm='/static/'+source_webm.textContent;
  else source_webm='';
  document.querySelector('#video_grande h1').innerHTML = alt;
  document.querySelectorAll('#video_grande p')[0].innerHTML = legende;
  document.querySelectorAll('#video_grande p')[1].innerHTML = date + '<br/>' + auteur + '<br/>' + lieu;
  var sources = document.querySelectorAll('#video_grande source');
  sources[0].setAttribute('src',source_mp4); 
  sources[1].setAttribute('src',source_ogg);
  sources[2].setAttribute('src',source_webm);
  document.querySelector('#video_grande video').setAttribute('poster',apercu);
  document.getElementsByTagName('video')[0].load();
 };

 /* associer aux videos le comportement au clic */
 for(var k=0;k<nb_v;k++)
 	{
 	document.querySelectorAll('a[href="#video_grande"]')[k].onclick=function(){
   		j=k-1;
   		changer_donnees_video(this);
  		};
  	};
 
 /* comportement du défilement droit au clic */
 document.getElementById('video-droite').onclick = function(){
  if (j<nb_v-1){
   j++;
  }
  else{
   j=0;
  }
  var video=document.querySelectorAll('a[href="#video_grande"]')[j];
  changer_donnees_video(video);
 };
 
 /* comportement du défilement gauche au clic */
 document.getElementById('video-gauche').onclick = function(){
  if (j>0){
   j--;
  }
  else{
   j=nb_v-1;
  }
  var video=document.querySelectorAll('a[href="#video_grande"]')[j];
  changer_donnees_video(video);
 };
 /* arrêt de la vidéo au clic */
 document.querySelector('#video_grande .retour-liste-img').onclick = function(){
  document.getElementsByTagName('video')[0].pause();
  document.getElementsByTagName('video')[0].currentTime = 0;
 };
};

