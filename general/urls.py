
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('general.views',
    url(r'^histoire$','histoire',name="histoire"),
    url(r'^regles$', 'reglement',name="reglement"),
    url(r'^contacts$','contact',name="contact"),
    url(r'^nous_sponsoriser$','sponsor',name="sponsor"),
    url(r'^mentions_legales$','mentions_legales',name="mentions"),
)