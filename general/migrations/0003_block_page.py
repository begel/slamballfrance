# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0002_auto_20150725_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='block',
            name='page',
            field=models.CharField(default='reglement', max_length=20),
            preserve_default=False,
        ),
    ]
