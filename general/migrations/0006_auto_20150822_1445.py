# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0005_sponsor_societe'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sponsor',
            name='genre',
            field=models.CharField(max_length=1),
        ),
        migrations.AlterField(
            model_name='sponsor',
            name='is_societe',
            field=models.CharField(max_length=3),
        ),
    ]
