# -*- coding: utf-8 -*-
from django import forms

class SponsorForm(forms.Form):
    nom = forms.CharField(max_length=50, label="Nom")
    prenom = forms.CharField(max_length=50, label="Prénom")
    genre = forms.ChoiceField(choices=[(Femme,'Femme'),(Homme,'Homme')], label="Sexe")
    is_societe = forms.ChoiceField(choices=[(soc,'Société'), (par,'Particulier')], label="Êtes-vous une société ou un particulier ?")
    societe = forms.CharField(max_length=100, label="Nom de votre société")
    mail = forms.EmailField(label="Adresse mail")
    message = forms.CharField(widget=forms.Textarea, label="Message")
