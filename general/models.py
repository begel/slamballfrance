
from django.db import models
from django.contrib import admin

class Block(models.Model):
    titre = models.CharField(null=True, blank=True, max_length=100)
    contenu = models.TextField()
    page = models.CharField(max_length=20)
    
    def __unicode__(self):
        return u'%s' %(self.titre)

class Sponsor(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    genre = models.CharField(max_length=1)
    is_societe = models.CharField(max_length=3)
    societe = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    mail = models.EmailField()
    message = models.TextField()
    
    def __unicode__(self):
        return u'%s' %(self.date)

