
# -*- coding: utf-8 -*-
from django import forms

class SponsorForm(forms.Form):
    nom = forms.CharField(max_length=50, label="Nom")
    prenom = forms.CharField(max_length=50, label="Prénom")
    genre = forms.ChoiceField(choices=[('F','Femme'),('H','Homme')], label="Sexe",widget=forms.RadioSelect)
    is_societe = forms.ChoiceField(choices=[('soc','Société'), ('par','Particulier')], label="Statut", widget=forms.RadioSelect)
    societe = forms.CharField(max_length=100, label="Nom de la société", required=False)
    mail = forms.EmailField(label="Adresse mail")
    message = forms.CharField(widget=forms.Textarea, label="Message")