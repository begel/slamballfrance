
# -*- coding: utf-8 -*-
from django.shortcuts import render
from general.models import Block
from general.models import Sponsor
from general.forms import SponsorForm

def mentions_legales(request):
    return render(request, 'general/mentions.html')

def reglement(request):
    blocks = Block.objects.filter(page="reglement")
    return render(request, 'general/reglement.html', locals())

def histoire(request):
    blocks = Block.objects.filter(page="histoire")
    return render(request, 'general/histoire.html', locals())

def contact(request):
    blocks = Block.objects.filter(page="contact")
    info = blocks[2]
    maxB = blocks[0]
    vinceR = blocks[1]
    return render(request, 'general/contact.html', locals())

def sponsor(request):
    blocks = Block.objects.filter(page="sponsor")
    if request.method == 'POST':  # S'il s'agit d'une requête POST
        form = SponsorForm(request.POST)  # Nous reprenons les données
        if form.is_valid(): # Nous vérifions que les données envoyées sont valides
            # Ici nous pouvons traiter les données du formulaire
	    nom = form.cleaned_data['nom']
	    prenom = form.cleaned_data['prenom']
	    genre = form.cleaned_data['genre']
	    is_societe = form.cleaned_data['is_societe']
            societe = form.cleaned_data['societe']
	    mail = form.cleaned_data['mail']
	    message = form.cleaned_data['message']
            data = Sponsor(nom=nom, prenom=prenom,genre=genre,is_societe=is_societe,societe=societe,mail=mail,message=message)
            data.save()
            # Nous pourrions ici envoyer l'e-mail grâce aux données que nous venons de récupérer
            envoi = True
            form = SponsorForm()
        else:
            incorrect=True
    else: # Si ce n'est pas du POST, c'est probablement une requête GET
        form = SponsorForm()  # Nous créons un formulaire vide
    return render(request, 'general/sponsor.html', locals())

