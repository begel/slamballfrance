
from django.contrib import admin
from general.models import Block,Sponsor

class BlockAdmin(admin.ModelAdmin):
   list_display   = ('titre', 'page',)
   list_filter    = ('page',)
   ordering       = ('page', )
   search_fields  = ('titre', 'page')

admin.site.register(Block, BlockAdmin)
admin.site.register(Sponsor)


